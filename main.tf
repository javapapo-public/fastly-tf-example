###################################### S3 BUCKET FOR TERRAFORM STATE #####################################
terraform {
  backend "s3" {
    region = "eu-west-1"

    #the bucket
    bucket = "papo-terraform-state"

    #the subfolder within the bucket for this stack
    key = "fastly-terraform-example/terraform.tfstate"
  }
}

# output varialbles on the console
output "Fastly-Version" {
  value = "${fastly_service_v1.fastly-terraform-demo.active_version}"
}

###################################### FASTLY ######################################################
resource "fastly_service_v1" "fastly-terraform-demo" {
  name          = "${var.fastly-service-name}"
  force_destroy = true
  default_ttl   = "${var.defaultTtl}"
  default_host  = "${var.s3-bucket-host}"

  #----------------------------------------- DOMAINS -----------------------------------------------

  domain {
    name    = "${var.domain}"
    comment = "main service for www.javapapo.eu"
  }
  backend {
    address = "${var.s3-bucket}"
    name    = "S3"
    port    = "80"
  }
  header {
    destination = "http.x-amz-request-id"
    type        = "cache"
    action      = "delete"
    name        = "remove x-amz-request-id"
  }
  gzip {
    name          = "file extensions and content types"
    extensions    = ["css", "js"]
    content_types = ["text/html", "text/css"]
  }
  vcl {
    name    = "Main"
    content = "${file("${path.module}/vcl/main.vcl")}"
    main    = true
  }
}
