variable "fastly-service-name" {
  default = "javapapo.eu"
}

##default cache time to live applied on Fastly service
variable "defaultTtl" {
  default = 60
}

variable "domain" {
  default = "www.javapapo.eu"
}

variable "s3-bucket" {
  default = "javapapoeu.s3-website.eu-west-1.amazonaws.com"
}

variable "s3-bucket-host" {
  default = "javapapoeu.s3-website-eu-west-1.amazonaws.com"
}
