sub vcl_recv {

  # if you want my blog- redirect to it!
  if(req.url.path ~ "^/blog"){
    error 666;
  }
  #FASTLY recv
}

sub vcl_fetch {

  # if I dont have the page get an EPIC Fail response
  if(beresp.status==404){
    error 667;
  }
#FASTLY fetch
}


sub vcl_error {
  if (obj.status == 666) {
    set obj.status = 307;
    set obj.http.Cache-Control = "no-cache";
    set obj.response = "Moved Temporarily";
    set obj.http.Location = "https://javapapo.blogspot.com";
    return (deliver);
  }

  if (obj.status == 667) {
    set obj.status = 404;
    set obj.response = "Not Found";
    set obj.http.Cache-Control = "no-cache";
    synthetic {"
      <html>
        <head>
        </head>
        <body>
          <h1>EPIC FAIL</h1>
        </body>
      </html>
    "};
    return(deliver);
  }

#FASTLY error
}