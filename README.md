# fastly-terraform-example
Sample project to illustrate how you can manage your [Fastly](https://www.fastly.com/) services with [terraform](https://www.terraform.io/)
an and specifically the [Fastly Provider](https://www.terraform.io/docs/providers/fastly/index.html) . The example terraforms a new Fastly Service, using a Fastly
private (free) account. The service is very simple, it proxies my personal domain `javapapo.eu` and using an S3 bucket as a backend serves, an index.html.
You can keep the basic structure of the example and ditch the rest of the example e.g that I use S3,my domain etc. If you want to do exactly the same  check the section
`Specifics of the example` below. The related blog post can be found [here](https://javapapo.blogspot.com/2018/12/terraform-your-fastly-service-simple.html)

Blog post : `https://javapapo.blogspot.com/2018/12/terraform-your-fastly-service-simple.html`

The main points of this example is

* Provide a sample structure to start with
* Illustrate the basics of the Fastly `terraform` provider
* Illustrate how you can add and upload `custom` VCL to your terraformed Fastly Service

### Pre Requisites (readme) -
 
 ##### You will need a Fastly Account. 
 
 Once you activate your account you will need to create a new an API TOKEN see [here](https://docs.fastly.com/guides/account-management-and-security/using-api-tokens). This token will be used by the `Terraform` provider, to `talk` to Fastly.

 You will need to export or provide to `terraform` this key!

 ```bash
 export FASTLY_API_KEY=XXXXXXXXXXXXXXXXXXXX
 ```
 
 ##### An S3 bucket (optional) if you want to save your state
 As you can see I am using a private S3 bucket to save my `terraform state`, you dont have to follow the same pattern so you can delete the following part. 
 You can rely on the local stated created on your machine. 

 ```terraform
 terraform {
  backend "s3" {
    region = "eu-west-1"
    #the bucket
    bucket = "papo-terraform-state"
    #the subfolder within the bucket for this stack
    key = "fastly-terraform-example/terraform.tfstate"
  }
}
```

In case you want to use an S3 bucket you will need to provide to `terraform` the AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY on your environment.

 ```bash
export AWS_DEFAULT_REGION=<YOUR_REGION>
export AWS_SECRET_ACCESS_KEY=<YOUR_SECRET_KEY>
export AWS_ACCESS_KEY_ID=<YOUR_SECRET_KEY_ID>
 ```
### Specifics of the example (in case you want to do exactly the same), register a domain/ s3 bucket hosting etc.

* I registered a domain on AWS, `www.javapapo.eu`
* I added a CNAME on my domain through the AWS console to point to Fastly (NON SSL) see [here](https://docs.fastly.com/guides/basic-setup/adding-cname-records#non-tls-hostnames-and-limiting-traffic)
* I created an S3 bucket (with the appropriate policies, so I can serve some static html)

### How to run

Assuming you have `FASTLY_API_KEY` at least exposed on your `env` and optionally `AWS_SECRET_ACCESS_KEY`, `AWS_ACCESS_KEY_ID` (if you want to store the tfstate) is simple as

```bash
terraform init
terraform plan
terraform apply
```
